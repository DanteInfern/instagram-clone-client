import React,{useContext} from 'react'
import {Link,useHistory} from  'react-router-dom'
import { UserContext } from "../App";
const NavBar = () => {
    const {state,dispatch} = useContext(UserContext)
    const history = useHistory() 
    const renderList = () => {
        if (state){
            return [
                <li key="profile"><Link to="/profile">Profile</Link></li>,
                <li key="create"><Link to="/create">Create Post</Link></li>,
                <li key="out">
                    <button className="waves-effect waves-light btn #c62828 red darken-3"
                        onClick={()=>{
                            localStorage.clear()
                            dispatch({type:"CLEAR"})
                            history.push("/signin")
                        }}
                    >                    
                        Logout
                    </button>
                </li>
            ]
        }else{
            return [
                <li key="login"><Link to="/signin">Login</Link></li>,
                <li key="signup"><Link to="/signup">Singup</Link></li>
            ]
        }
    }
    return (        
        <nav>
            <div className="nav-wrapper white">
                <Link to={state?"/":"/signin"} className="brand-logo left">Instagram</Link>
                <ul id="nav-mobile" className="right hide-on-med-and-down">                    
                    {renderList()}
                </ul>
            </div>
        </nav>      
    );
}

export default NavBar